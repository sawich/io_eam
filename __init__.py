
bl_info = {
    'name': 'Ethereal Angel Mesh format',
    'author': 'Sawich',
    'version': (1, 0, 0),
    'blender': (2, 80, 0),
    'location': 'File > Import-Export',
    'description': 'Import-Export Ethereal Angel Mesh',
    'warning': '',
    'wiki_url': 'http://angeldev.org',
    'support': 'COMMUNITY',
    'category': 'Import-Export'
}

if 'bpy' in locals():
	import importlib
	if 'import_eam' in locals():
		importlib.reload(import_eam)
	if 'export_eam' in locals():
		importlib.reload(export_eam)

import bpy

from bpy.props import(
	StringProperty,
	BoolProperty
)

from bpy_extras.io_utils import(
	ExportHelper,
	orientation_helper,
	axis_conversion,
)

@orientation_helper(axis_forward='-Z', axis_up='Y')
class ExportEAM(bpy.types.Operator, ExportHelper):
	bl_idname  = 'export_mesh.eam'
	bl_label   = 'Export EAM'
	bl_options = { 'PRESET' }

	filename_ext = '.eam'

	filter_glob: StringProperty(
		default = '*.eam',
		options = { 'HIDDEN' }
	)

	# use_selection: BoolProperty(
	# 	name        = 'Selection Only',
	# 	description = 'Export selected objects only',
	# 	default     =  False,
	# )

	use_triangles: BoolProperty(
		name        = 'Triangulate Faces',
		description = 'Convert all faces to triangles',
		default     =  False,
	)

	check_extension = True

	def execute(self, context):
		from .         import export_eam
		from mathutils import Matrix

		keywords = self.as_keywords(ignore = (
			'axis_forward', 'axis_up', 'check_existing', 'filter_glob'
		))

		global_matrix = (Matrix.Scale(1.0, 4) @ axis_conversion(
			to_forward = self.axis_forward,
			to_up = self.axis_up,
		).to_4x4())
		####
		keywords['global_matrix'] = global_matrix

		return export_eam.save(context, **keywords)

def menu_func_export(self, context):
	self.layout.operator(ExportEAM.bl_idname, text = 'Ethereal Angel Mesh (.eam)')

classes = (
	ExportEAM,
)

def register():
	for cls in classes:
		bpy.utils.register_class(cls)

	bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
	bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

	for cls in classes:
		bpy.utils.unregister_class(cls)

if '__main__' == __name__:
	register()
