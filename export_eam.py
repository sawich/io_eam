
import os

import bpy

from mathutils          import Matrix, Vector, Color
from bpy_extras         import io_utils, node_shader_utils
from mathutils.geometry import normal

from bpy_extras.wm_utils.progress_report import(
	ProgressReport,
	ProgressReportSubstep,
)

def faces_from_mesh(ob, global_matrix):
	ob.update_from_editmode()
	depsgraph = bpy.context.evaluated_depsgraph_get()
	mesh_owner = ob.evaluated_get(depsgraph)

	try:
		mesh = mesh_owner.to_mesh()
	except RuntimeError:
		return

	mat = global_matrix @ ob.matrix_world
	mesh.transform(mat)

	if mat.is_negative:
		mesh.flip_normals()

	mesh.calc_loop_triangles()
	vertices = mesh.vertices

	for tri in mesh.loop_triangles:
		yield [vertices[index].co.copy() for index in tri.vertices]

	mesh_owner.to_mesh_clear()

def write_file(filepath, faces):
	print('filepath: %s' % filepath)

	with open(filepath, 'w') as f:
		for face in faces:
			fw = f.write
			# calculate face normal
			fw('facet normal %f %f %f\nouter loop\n' % normal(*face)[:])
			for vert in face:
					fw('vertex %f %f %f\n' % vert[:])
			fw('endloop\nendfacet\n')
	# with open(filepath, 'w') as f:
	# 	fw = f.write

	# 	fw(b'test')

	return {'FINISHED'}

def _write(context, filepath, global_matrix):
	base_name, ext = os.path.split(filepath)
	context_name = [ base_name, '', '', ext]

	scene = context.scene

	if(bpy.ops.object.mode_set.poll()):
		bpy.ops.object.mode_set(mode = 'OBJECT')

	frame_current = scene.frame_current

	# object = context.selected_objects[0]
	ob = context.active_object

	full_path = '\\'.join(context_name)

	faces = faces_from_mesh(ob, global_matrix)

	return write_file(full_path, faces)


def save(context, filepath, *, use_triangles = False, global_matrix = None):
	if global_matrix is None:
		global_matrix = Matrix()

	_write(context, filepath, global_matrix)
	return { 'FINISHED' }
