import os
import bpy
import bmesh
import struct

for object in bpy.context.scene.objects:
    with open('Y:\\vsprojects\\as_new\\bin\\' + object.name + '.txt', 'wb') as file:
        object.update_from_editmode()

        me = object.data
        bm = bmesh.new()
        bm.from_mesh(me)

        bmesh.ops.triangulate(bm, faces=bm.faces)
        bm.to_mesh(me)

        me.calc_normals_split();
        uv_layer = me.uv_layers.active.data

        uv_layer = bm.loops.layers.uv.active
        if uv_layer is None:
            raise Exception("No active UV map (uv)")

        lines = []
        for face in bm.faces:
            for loop, vert in zip(face.loops, face.verts):
                lines.append([vert.co[0], vert.co[1], vert.co[2], me.loops[loop.index].normal[0], me.loops[loop.index].normal[1], me.loops[loop.index].normal[2], loop[uv_layer].uv[0], loop[uv_layer].uv[1]])

        vertices = []
        indices = []
        for i, v in enumerate(lines):
            to_insert = i
            for dup_idx in range(0, i):
                if lines[dup_idx] == v:
                    vertices.append(v)
                    to_insert = dup_idx
                    break
            indices.append(to_insert)

        file.write(struct.pack('ii', vertices.__len__(), indices.__len__()))
        for v in vertices:
            file.write(struct.pack('f'*len(v), *v))

        file.write(struct.pack('i'*len(indices), *indices))

        bm.free()
        del bm
